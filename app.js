const express = require('express'); // подключаем библиотеку экспресс
const mongoose = require('mongoose'); // подключаем систему управления базой данных MongoDB
const sliderRoutes = require('./routes/slider'); // подключаем файл с роутингом для слайдера
const tariffPlansRoutes = require('./routes/tariffPlans'); // подключаем файл с роутингом для тарифных планов
const applicationRoutes = require('./routes/application'); // подключаем файл с роутингом для заявок
const keys = require('./config/keys') // подключаем конфиг с ключами для соединения с базой данных.
const app = express(); // инициализируем приложение от фреймворка экспресс

// регистрируем роутинг
app.use('/api/slider', sliderRoutes)
app.use('/api/tariffPlans', tariffPlansRoutes)
app.use('/api/application', applicationRoutes);

module.exports = app; // экспортируем наше приложение