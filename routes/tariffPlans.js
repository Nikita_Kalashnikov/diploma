const express = require('express');
const router = express.Router();
const controller = require('../controllers/tariffPlans');

router.get('/', controller.getTariffPlans);

module.exports = router;