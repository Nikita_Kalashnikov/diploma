const express = require('express');
const router = express.Router();
const controller = require('../controllers/application');

router.post('/', controller.getTariffPlans);

module.exports = router;